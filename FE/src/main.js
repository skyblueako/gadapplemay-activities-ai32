import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue'
import './assets/css/style.css'

import Book from './components/pages/Book.vue'

import "chart.js"
import "hchs-vue-charts"

Vue.use(window.VueCharts);

Vue.config.productionTip = false

Vue.config.productionTip = false

new Vue({
  render: h => h(Book),
}).$mount('#app')
